#! /usr/bin/env Rscript
# Remueve todos los objetos creados
require(ggplot2)
library(scales)
library(ggpmisc)

rm(list=ls())
args <- commandArgs(trailingOnly = TRUE)
E <- c()

#Creo las matrices que contendran mis valores
V <- c()
W <- c()


#Esta funcion es para agregar los items a cada matriz de valores
push <- function(vec, item){
    vec=substitute(vec)
    eval.parent(parse(text = paste(vec, ' <- c(', vec, ', ', item, ')', sep = '')), n = 1)
}

#Esta funcion lee los valores y llena las matrices
myfunt <- function(){
  cat("Cuantos pares de valores quieres ingresar : ")
  totas <- as.numeric(readLines(con = file("stdin")))

  for (n in 1:totas) {
    cat("Ingresar el Par ",paste(n),":\n")
    cat("X",paste(n),":\n")
    xi <- as.numeric(readline(prompt = ""))
    cat("Y",paste(n),":\n")
    yi <- as.numeric(readline(prompt = ""))
    push(V,xi)
    push(W,yi)
  }

  cat("Valores en V:\n"); str(V)
  cat("Valores en W:\n"); str(W)
 }


#if (action == "--args") {
if (length(args) == 0) {
    cat("No hay argumentos a evaluar\n");
} else {
    for (f in 1:length(args)) {
        print(args[f])
        #E[f]=args[f]
        A <- unlist(strsplit(args[f], split=" "))
        for (i in 1:length(A)) {
            push(V,A[i])
        }
    }

}
print(V)

B <- data.frame(x=E[1],y=E[2])
print(B)





#if (interactive()) myfunt()
