#!/usr/bin/env python
# -*- coding: utf-8 -*-
#========================================
# Nombre    : ProyectoMetodosNumericos.py
# Desarrollo: Mauricio Martinez Garcia
# Variables :
# Ejemplo   :
#Descripcion
#*****************************************************************
#*****************************************************************
__author__="morfeokmg@gmail.com"
__date__ ="Aug 08, 2018 13:11:45"
__docformat__ = 'epytext en'

#librerias basicas
import re
import sys
import os
import pdb
#import random
import optparse
from optparse import OptionParser

class commandArgs(object):
    """
        Clase Principal de mi proceso para el proyecto de Metodos numericos.
        Con este codigo se pretende ejecutar el archivo de template asi como el .r
        en una sola linea de comando para generar el calculo y el pdf
    """
    
    def __init__(self):
        """
            Se inicializan las variables principales del API. 
             
            Variables Globales para la Ayuda, los errores comunes y los parametros
            que se obtienen del archivo de configuracion.            
            
            @param help_1: parametros de ayuda para la opcion -x (x_axis)
            @param help_2: parametros de ayuda para la opcion -y (y_axis)

        """
        global help_1
        global help_2
        
        #Lineas que se agregaran en la ayuda del programa python
        help_1 = """Parametros para el eje ordenado X que seran ingresados (x_axis opcion -x)""" 
                    
        help_2 = """Los parametros para el eje ordenado Y que seran ingresados  (y_axis opcion -y) """

        
        #Termina listado de lineas de Ayuda.

    def is_number(self,s):
        try:
            float(s)
            return True
        except ValueError:
            pass
        
        try:
            import unicodedata
            unicodedata.numeric(s)
            return True
        except (TypeError, ValueError):
            pass
    
        return False
        

    def invoqueRCran(self):
        """test"""
        pass


    def creaArchivo(self, cami,nombre):
        fullName = os.path.join(cami, nombre)                
        try:
            cursArchivo = open(fullName,'w')        
            return cursArchivo
        except IOError as FileERROR:
            exit(1)

    def abrirArchivo(self,outpath,nombre,flag):
        fullName = os.path.join(outpath, nombre)
        try:
            cursArchivo = open(fullName,flag)
            return cursArchivo
        except IOError as FileERROR:
            exit(1)

    def cierraArchivo(self, FILE):
        try:            
            FILE.close()
        except IOError as (errno, strerror):            
            sys.exit(1)            

            
    def docAPI(self):
        """
        Documento de Ayuda para el manejo de la interfaz de comandos.
        

        SYNOPSIS 
        ========
        
        ProyectoMetodosNumericos.py [OPTIONS]
        

        DESCRIPCION
        ===========
        
        Este documento describe el uso de la interfaz cliente de consola 
        para poder operar la herramienta de obtencion del calculo de la ecuación a través del método 
        de Ajuste de Lineas por el método Polinomial
        Si se requiere ayuda, usar la opcion (-h o --help).
        

        EJEMPLOS DE USO
        ===============
        
        A. Paso de parámetros: 
        ============================================
                
        Se requerirán los valores de los pares ordenados de acuerdo a los parámetros x,y
        de forma que sean organizados y pasados como pares ordenados, deberán ser al menos 2 pares de valores.
        Los parámetros X y Y ambos son mandatorios: 
        
        ['-x','--x_axis']            
                
                - B{Parametro mandatorio}                
                
                >>> ./ProyectoMetodosNumericos.py -x
                
                    - B{Ejemplo}: 
                
                    >>> ./ProyectoMetodosNumericos.py -x '1,-2,-0.34,1.23'

        ['-y','--y_axis']            
                
                - B{Parametro mandatorio}                
                
                >>> ./ProyectoMetodosNumericos.py -y
                
                    - B{Ejemplo}: 
                
                    >>> ./ProyectoMetodosNumericos.py -y '1,-2,-0.34,1.23'


        - B{Ejemplo de uso completo}: 
              
                    >>> ./ProyectoMetodosNumericos.py -x '1,-2,-0.34,1.23' -y '1,-2,-0.34,1.23'
    
                    
                - B{I{NOTA}}: I{No se puede definir solamente el par X, o Y de forma individual, 
                       siempre se deben recibir los 2 valores dado que son pares ordenados}
                    
                                    
        ================================================================================================
        

        NOTAS: 
        ======
        
        El proceso generará un archivo PDF como documento final con el formato definido en la rubrica del proyecto
        con el calculo requerido, la grafica y los valores ingresados 
        

        AUTHOR:
        =======
        
        Name: <morfeokmg@gmail.com>
        

        LICENCIA:
        =========
        
        Este programa ha sido disenado bajo el requerimiento del proyecto de Metodos numericos, el codigo  
        y la logica son propiedad del autor y se regira bajo la licencia GPL v2        
        ================================================================================================
        """            
        #return self.docAPI.__doc__


        
    def optionsParser(self, arg):
        """
            Metodo que parsea las opciones pasadas como parametro. 
            
            @param arg: Son los parametros ingresados al comando a traves de la consola de la forma 
                        [OPTIONS] [DATA] [PARAMETERS].
            
            @cvar fmt: Formato en el que se presentara la ayuda.
                                                 
        """
        fmt = optparse.IndentedHelpFormatter(indent_increment=4, max_help_position=160, width=177, short_first=1 )
        #parser = OptionParser(formatter=optparse.TitledHelpFormatter(),usage="%prog [OPTIONS] [DATA] [PARAMETERS]", version="%prog 2.0.1 commit 841fee87c04a0965fd922e489ac9b26f365689ba")
        parser = OptionParser(formatter=fmt,usage="%prog [PARAMETERS]", version="%prog 4.0.5 commit 981f5323e5cc0dda4a72a2c0bdef15ef7f914b05")
        fmt
        #parser = OptionParser(formatter=optparse.TitledHelpFormatter(),usage=globals()['__doc__'], version="%prog 2.0.10 commit ")                    
        parser.add_option("-x","--x_axis",dest="x_axis",help=help_1,metavar="<X VALUES>")
        parser.add_option("-y","--y_axis",dest="y_axis",help=help_2,metavar="<Y VALUES>")        
        
                
        (options,args) = parser.parse_args(arg)
        #print 'options: %s, args: %s' % (options,args)
        
        #=================================================================
        #Validacion de las opciones a parsear. 
        #=================================================================                   
        if not (options.x_axis and options.y_axis):
            print self.docAPI.__doc__
            parser.error ('Debe definir una opcion de accion..')
                        
        else:
    #=============================================================================
    #se valida que se tengan ambas opciones de parametros
    #=============================================================================            
            if (options.x_axis and options.y_axis):                
                #Validamos si los valores ingresados en los parametros son numeros validos.
                X = options.x_axis.split(",")
                Y = options.y_axis.split(",")
                validacion = commandArgs()

                print "analizando los valores que se pasaron como parametros..."
                xValue = [x for x in X if validacion.is_number(x)]
                yValue = [y for y in Y if validacion.is_number(y)]

                if not (len(xValue) == len(yValue)):
                    print "Se pasaron valores no numericos, o no se obtuvieron todos los pares ordenados"
                    print "A continuacion los valores ingresados"
                    print xValue, yValue
                    exit
                if not (len(xValue) > 0 or len(yValue) > 0):
                    print "No se ingresaron pares ordenados requeridos para poder hacer el calculo"
                    print "A continuacion los valores ingresados"
                    print xValue, yValue
                    exit

                print "Valores ingresados: "
                print xValue, yValue

                print "Generando archivo temporal con las coordenadas en formato tabular..."
                defpath = os.getcwd()
                FileA = validacion.creaArchivo(defpath,"maulist.tmp")
                for x in range(0,len(xValue)):                        
                    FileA.write(xValue[x]+'|'+yValue[x]+os.linesep)
                FileA.close()

                #va a abrir los archivos de templates de LaTeX para generar el completo
                FileHeader = validacion.abrirArchivo(defpath,"header_latex.template","r").read()
                FileFooter = validacion.abrirArchivo(defpath,"footer_latex.template","r").read()

                #Ejecuta el script de R
                print "Ejecutando el script R para el calculo de la ecuación polinomial..."
                A = os.system("./ProyectoMetodosNumericos_v3.r > /dev/null 2>&1 ")
                if (A == 0):
                    print "Se ejecutó correctamente el cálculo, generando estructura PDF..."
                else:
                    print "Hubo un problema con el script R por lo que se finalizara el proceso..."
                    exit(1)
                    
                #Lee las imagenes generadas en la ruta 
                listaImagenes = os.listdir("images/")
                listaImagenes.sort()

                FileLatex = validacion.creaArchivo(defpath,"metodosNumericos.tex")
                FileLatex.write(FileHeader)
                print "Generando script LaTeX con los parametros de las graficas realizadas y los estadisticos calculados...\n"
                t = 2
                for i in range(0,len(listaImagenes)):
                    lineOfLaTeX="""\n \section{Ejercicio Calculado Manualmente de Grado %d }
 A continuaci\\'on, de los valores ingresados, se generar\\'an las gr\\'aficas calculando a partir de un polin\\'omio de segundo grado hasta \\(n\\). Donde \\(n\\) es el grado m\\'aximo usado debido a la cantidad de valores ingresados (longitud de X = longitud Y).

\\begin{figure}[!htb]
\\begin{center}
\includegraphics[width=3in]{images/%s}
\caption{Funci\\'on de la gr\\'afica %d}
\label{fig1}
\end{center}
\end{figure}
                    \n""" % (int(t),str(listaImagenes[i]),int(i))
                    t +=1
                    FileLatex.write(lineOfLaTeX)

                FileLatex.write(FileFooter)
                FileLatex.close()
                
                #lineaDeArchivo = ArchivoAConsultar.readline()
                print "Uniendo Archivos Latex y la bibliografía APA..."
		W = os.system("pdflatex metodosNumericos > /dev/null 2>&1 ")
                if (W == 0):
                    print "Se ejecutó correctamente el cálculo, generando estructura PDF..."
		else:
                    print "Hubo un problema en la ejecución de LaTeX por lo que se finalizara el proceso..."
                    exit(1)

		O = os.system("bibtex metodosNumericos > /dev/null 2>&1 ")
                if (W == O):
                    print "Se genero el indice bibliográfico correctamente..."
                else:
                    print "Hubo un problema en la ejecución de bibtex por lo que se finalizara el proceso..."
                    exit(1)                    

                W = os.system("pdflatex metodosNumericos > /dev/null 2>&1 ")
                if (W == 0):
                    print "Se generó correctamente el archivo PDF final..."
                else:
                    print "Hubo un problema en la finalización y cierre del proceso..."
                    exit(1)                    

                print "Cerrando y limpiando proceso..."
                os.system("rm metodosNumericos.tex metodosNumericos.aux metodosNumericos.log maulist.tmp metodosNumericos.blg metodosNumericos.bbl  images/*")
                exit(0)
                    
#========================================================================
#========================================================================
#========================================================================
#========================================================================                    
if __name__ == "__main__" :
    #pdb.set_trace()
    optione = sys.argv[1:]
    #print "Iniciando"
    abreAPI = commandArgs()
    abreAPI.optionsParser(optione)
    #print "prueba"
    
