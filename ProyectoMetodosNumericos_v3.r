#! /usr/bin/env Rscript
#---
#title: "Ajuste Polinómico"
#author: "Mauricio Martinez Garcia"
#date: "August 1, 2018"
#output: pdf_document
#---

# Remueve todos los objetos creados
suppressPackageStartupMessages(require(ggplot2,quietly=TRUE))
suppressPackageStartupMessages(library(scales,quietly=TRUE))
suppressPackageStartupMessages(library(ggpmisc,quietly=TRUE))
#suppressPackageStartupMessages(library(rticles,quietly=TRUE))


rm(list=ls())

#Creo las matrices que contendran mis valores
V <- c()
W <- c()
n <- 0


#Esta funcion es para agregar los items a cada matriz de valores
push <- function(vec, item){
    vec=substitute(vec)
    eval.parent(parse(text = paste(vec, ' <- c(', vec, ', ', item, ')', sep = '')), n = 1)
}

inc <- function(x)
{
 eval.parent(substitute(x <- x + 1))
}

createGraphic <- function(v1,v2,dat,ec,grad){
    library(grid)
    library(gridExtra)
    
    p1 <- ggplot(ec, aes(y=dat$y, x=dat$x)) + geom_point(alpha = .3) + labs(x="x",y="y", colour = "Cylinders",shape = "Transmission") + stat_smooth(method = "lm", formula = y ~ poly(x,grad))
    tt <- ttheme_default(colhead=list(fg_params = list(parse=TRUE)))
    tt1 <- ttheme_default()
    tt2 <- ttheme_minimal()
    
    #pdf("AnalysisOf_PolinomialAdjus.pdf", title = "Mau test")
    d <- head(summary(dat))
    Stats <- lm(dat)
    finalGrap <- p1 + stat_poly_eq(aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
                      label.x.npc = "right", label.y.npc = 0.15,
                      formula = y ~ poly(x,grad), parse = TRUE, size = 4)
    
    grid.arrange(
        tableGrob(dat,theme=tt),
        tableGrob(d,theme=tt),        
        finalGrap,
        tableGrob(c("Residuals",Stats$residuals),theme=tt),
        tableGrob(c("Coefficients",Stats$coefficients),theme=tt),
        nrow=3)
    
    #dev.off()

}

#================================================================================
#================================================================================
#Evaluo los argumentos que se pasaron al comando via consola
#print(length(args))
#print(args)           
d = read.table('maulist.tmp', header=F, sep="|", col.names=c("x", "y"))
V <- d$x
W <- d$y

#Evalua si los grupos de valores son de la misma cantidad, no pueden ser diferentes.
if (! length(W) == length(V)){
    cat ("La cantidad de pares ordenados no puede ser distinta \n")
    cat("Valores de X: ",length(V),"\nValores de Y: ",length(W))
    quit(status=1)
}    


n <- length(W)-1
B <- data.frame(x=V,y=W)

fit <- lm(y ~ poly(x, n), data = B)
pred <- predict(fit)
summary (fit)

#pdf("AnalysisOf_PolinomialAdjus.pdf", title = "Mau test")

k <- 1
for (i in 2:n){
    ImageName = paste("images/AnalysisOf_PolinomialAdjus_",k,".png")
    png(ImageName)
    createGraphic(V,W,B,fit,i)
    inc(k)
    #grid.newpage()
}

dev.off()
