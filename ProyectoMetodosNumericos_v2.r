#! /usr/bin/env Rscript
#---
#title: "Ajuste Polinómico"
#author: "Mauricio Martinez Garcia"
#date: "August 1, 2018"
#output: pdf_document
#---

# Remueve todos los objetos creados
suppressPackageStartupMessages(require(ggplot2,quietly=TRUE))
suppressPackageStartupMessages(library(scales,quietly=TRUE))
suppressPackageStartupMessages(library(ggpmisc,quietly=TRUE))

rm(list=ls())
args <- commandArgs(trailingOnly = TRUE)
#args <- commandArgs()

#Creo las matrices que contendran mis valores
V <- c()
W <- c()
n <- 0


#Esta funcion es para agregar los items a cada matriz de valores
push <- function(vec, item){
    vec=substitute(vec)
    eval.parent(parse(text = paste(vec, ' <- c(', vec, ', ', item, ')', sep = '')), n = 1)
}


createGraphic <- function(v1,v2,dat,ec,grad){
    library(grid)
    library(gridExtra)
    
    p1 <- ggplot(ec, aes(y=dat$y, x=dat$x)) + geom_point(alpha = .3) + labs(x="x",y="y", colour = "Cylinders",shape = "Transmission") + stat_smooth(method = "lm", formula = y ~ poly(x,grad))
    tt1 <- ttheme_default()
    tt2 <- ttheme_minimal()
    
    pdf("sample.pdf", title = "Mau test")
    d <- head(summary(dat))

    #grid.newpage()
    p1 + stat_poly_eq(aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
                      label.x.npc = "right", label.y.npc = 0.15,
                      formula = y ~ poly(x,grad), parse = TRUE, size = 3)
    
    grid.arrange(
        tableGrob(dat,theme=tt1),
        tableGrob(d,theme=tt2),        
        p1,
        nrow=2)
    
    dev.off()

}


#Evaluo los argumentos que se pasaron al comando via consola
#print(length(args))
#print(args)
if (length(args) == 0) {
    cat("No hay argumentos a evaluar\n")
} else {    
    for (f in 1:length(args)) {
        #print(args[f])
        if (args[f] == "--help"){
            cat ("Usage: ProyectoMoetodosNumericos_v2.r [options]
                 ############################################################
                 ############################################################
                 This software is under GPL v3 License
                 CopyLeft @Mauricio Martinez Garcia
                 ############################################################
                 ############################################################
                 Options:
                       To give the pairs (x,y) you need to complete as: 
                       'xi,..,xn', Is the first group of values - for X
                       'yi,...,yn', Is the second group of values - for Y
                       -h, --help
                            Show this help message and exit
                 Examples:
                       ProyectoMetodosNumericos_v2.r '1,2' '0,5'
                       ProyectoMetodosNumericos_v2.r '1' '-1'
                       ProyectoMetodosNumericos_v2.r '1,1,1,1,1,1' '2,2,2,2,2,2'
                 ############################################################
                 ############################################################
            \n")
            break
           
        }else{
            #Aqui divide los valores de texto y los pasa a una lista de valores
            if (args[f] == "--x") {
                A <- unlist(strsplit(args[f+1], split=","))
                for (i in 1:length(A)) {
                    push(V,A[i])
                    
                }
                next
            }
            if (args[f] == "--y") {
                B <- unlist(strsplit(args[f+1], split=","))
                for (i in 1:length(B)) {
                    push(W,B[i])
                    
                }
                next
            }                 
       }
    }
}

#Evalua si los grupos de valores son de la misma cantidad, no pueden ser diferentes.
if (! length(W) == length(V)){
    cat ("La cantidad de pares ordenados no puede ser distinta \n")
    cat("Valores de X: ",length(V),"\nValores de Y: ",length(W))
    quit(status=1)
}    

n <- length(W)-1
B <- data.frame(x=V,y=W)


fit <- lm(y ~ poly(x, n), data = B)
pred <- predict(fit)
summary (fit)


#graficamos
#p1 <- ggplot(fit, aes(y=B$y, x=B$x)) +
#    geom_point(alpha = .3) + labs(x="x",y="y", colour = "Cylinders",shape = "Transmission") + stat_smooth(method = "lm", formula = y ~ poly(x,n))

#p1 + stat_poly_eq(aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
#               label.x.npc = "right", label.y.npc = 0.15,
#               formula = y ~ poly(x,3), parse = TRUE, size = 3)


createGraphic(V,W,B,fit,n)

